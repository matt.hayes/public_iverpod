# Public_Iverpod

A collection of utilities, tools, functions and models that I have developed on and off for ~8 years. This original project, while I was in graduate school, is basically how I taught myself what we know today as "data science". Originally written in R, because that was what I knew, this project led me to understand APIs, big data, memory and hardware constraints, parallel programing, cloud computing, task scheduling, job schedulers, optimizing for narrow results, extreme feature creation and many more. I have actively traded using these tools and made (and lost) money. Any models I am actively developing will not appear here.

# Building the Docker Container
The docker container is large but is 100% reproducible and has been for ~1.5 years on windows, mac and linux machines.

To Build the container, clone the repo and then:

```
cd {public_iverpod_directory}
docker build -t iverpod -f Dockerfile .
docker run -it -v $PWD:/scratch {-v localstoragepath:incontainerpath} --gpus all iverpod
```

As you can tell, this container is setup to use GPUs. This requires that you have an Nvidia-based GPU system and the drivers appropriately installed. It can be a real bear to set that up; while it is not the focus of putting this up, I may write detailed instructions but Google would likely cover you.

Now, once in the container, `python` will drop you in to a Python 3 terminal and you are ready to go. I typically attach my code repo volume into the container using the `-v` argument; this allows me to work in the terminal while I am typing code in my IDE. I prefer this method over notebooks but your mileage may vary.
