FROM nvidia/cuda:11.0-runtime-ubuntu20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/New_York

RUN mkdir /scratch && \
    mkdir -p /data/shared && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git libpq-dev gcc zip wget curl bzip2 unzip locales build-essential libgl1-mesa-glx python3-pip ca-certificates libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev nano libpq-dev openssl gnupg1 gnupg2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update -y && \
    apt-get upgrade -y && \
    ln -s -f /usr/bin/pip3 /usr/bin/pip && \
    ln -s -f /usr/bin/python3 /usr/bin/python

COPY pip_requirements.txt /scratch/pip_requirements.txt

RUN pip install --no-cache-dir -r /scratch/pip_requirements.txt

RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz && \
  tar -xvzf ta-lib-0.4.0-src.tar.gz && \
  cd ta-lib/ && \
  ./configure --prefix=/usr && \
  make && \
  make install

RUN pip install TA-Lib==0.4.19

RUN apt-get update -y && \
    apt-get upgrade -y

ENV PYTHONPATH=$PYTHONPATH:/scratch

COPY . /scratch
WORKDIR /scratch
