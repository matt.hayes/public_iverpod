import pandas as pd
import quandl
import json
import datetime
from datetime import date, timedelta
import tempfile
import subprocess
import os
from concurrent.futures import ProcessPoolExecutor

def get_quandl_key():
    keyfile = open('utils/secrets.json')
    keys = json.load(keyfile)
    quandl.ApiConfig.api_key = keys['QuandlAPIKey']


def get_all_data():
    with tempfile.TemporaryDirectory() as temp_dir:
        quandl.bulkdownload("EOD", filename=f"{temp_dir}/zippy.zip")
        subprocess.check_call(
            f"unzip -q {temp_dir}/zippy.zip -d {temp_dir}", shell=True
        )
        path = f"{temp_dir}/{os.listdir(temp_dir)[1]}"
        df = pd.read_csv(path, header=None)
        df = df.iloc[:, [0, 1, 9, 10, 11, 12, 13]]
        df.columns = ["Ticker", "Date", "Open", "High", "Low", "Close", "Volume"]
        df["Date"] = pd.to_datetime(df["Date"])
        # TODO: export to DB or return as pandas
        df = df.astype(
            {
                "Open": "float32",
                "High": "float32",
                "Low": "float32",
                "Close": "float32",
                "Volume": "float32",
            }
        )
        df.to_parquet(f"data/HistDump_{str(date.today())}.parquet")
        return f"data/HistDump_{str(date.today())}.parquet"


def get_all_data_fresh():
    with tempfile.TemporaryDirectory() as temp_dir:
        quandl.bulkdownload("EOD", filename=f"{temp_dir}/zippy.zip")
        subprocess.check_call(
            f"unzip -q {temp_dir}/zippy.zip -d {temp_dir}", shell=True
        )
        path = f"{temp_dir}/{os.listdir(temp_dir)[1]}"
        df = pd.read_csv(path, header=None)
        df = df.iloc[:, [0, 1, 9, 10, 11, 12, 13]]
        df.columns = ["Ticker", "Date", "Open", "High", "Low", "Close", "Volume"]
        df["Date"] = pd.to_datetime(df["Date"])
        df = df.astype(
            {
                "Open": "float32",
                "High": "float32",
                "Low": "float32",
                "Close": "float32",
                "Volume": "float32",
            }
        )
        # TODO: export to DB or return as pandas
        return df


def get_recent_data(tickers=["XOM", "AAPL", "BAC", "GE", "F"], daysback=10, n=8):
    proc_list = [[x, daysback] for x in tickers]
    with ProcessPoolExecutor(n) as ex:
        iterouts = ex.map(ind_tick_download, proc_list)
    outs = [x for x in iterouts]
    outs = [x for x in outs if x is not None]
    outs = pd.concat(outs)
    return outs


def ind_tick_download(x):
    ticker = x[0]
    daysback = x[1]
    pdint = quandl.get(
        f"EOD/{ticker}",
        start_date=f"{date.today() - timedelta(days=daysback)}",
    )
    pdint["Ticker"] = ticker
    pdint = pdint.reset_index()
    pdint = pdint[
        ["Ticker", "Date", "Adj_Open", "Adj_High", "Adj_Low", "Adj_Close", "Adj_Volume"]
    ]
    pdint.columns = ["Ticker", "Date", "Open", "High", "Low", "Close", "Volume"]
    pdint = pdint.astype(
        {
            "Open": "float32",
            "High": "float32",
            "Low": "float32",
            "Close": "float32",
            "Volume": "float32",
        }
    )
    return pdint


def get_selected_tick_data(
    quarter=None, daysback=10, n=8, tick_sel_path="data/sels.parquet"
):
    """
    deprecated
    """
    df = pd.read_parquet(tick_sel_path)
    if quarter is None:
        cur_quarter = str(df["Quarter"].max())
    else:
        cur_quarter = quarter
    ticks = df[df["Quarter"] == cur_quarter]["Ticker"].to_list()
    ticks = list(set(ticks))
    return get_recent_data(tickers=ticks, daysback=daysback)


def get_api_data(
    tickers=["XOM", "AAPL", "BAC", "GE", "F"], targdate=None, daysback=10, n=8
):
    proc_list = [[x, targdate, daysback] for x in tickers]
    with ProcessPoolExecutor(n) as ex:
        iterouts = ex.map(ind_tick_download_api, proc_list)
    outs = [x for x in iterouts]
    outs = [x for x in outs if x is not None]
    outs = pd.concat(outs)
    return outs


def ind_tick_download_api(x):
    ticker = x[0]
    targdate = x[1]
    daysback = x[2]
    targdate = datetime.datetime.strptime(targdate, "%Y-%m-%d").date()
    pdint = quandl.get(
        f"EOD/{ticker}",
        end_date=f"{targdate}",
        start_date=f"{pd.to_datetime(targdate) - timedelta(days=daysback)}",
    )
    pdint["Ticker"] = ticker
    pdint = pdint.reset_index()
    pdint = pdint[
        ["Ticker", "Date", "Adj_Open", "Adj_High", "Adj_Low", "Adj_Close", "Adj_Volume"]
    ]
    pdint.columns = ["Ticker", "Date", "Open", "High", "Low", "Close", "Volume"]
    pdint = pdint.astype(
        {
            "Open": "float32",
            "High": "float32",
            "Low": "float32",
            "Close": "float32",
            "Volume": "float32",
        }
    )
    return pdint
